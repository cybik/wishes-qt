//
// Created by cybik on 24-06-27.
//

#pragma once

#include <QString>

#include <util/log.h>

static const QString APP_VERSION = "0.0.1";

#define APPLICATION_NAME_GENERATOR(X) "moe.cybik.wishes"#X
